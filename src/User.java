import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/* Fancy User Interface- User
 * class of User Object for the Fancy User Interface
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 24.05.2021
 */
public class User {
	
	private String username;
	private String mailadress;
	private String password;
	private boolean isAdmin;
	
	public static int userCount=0;
	
	public User(String username, String mailadress, String password, boolean isAdmin) {
		this.username = username;
		this.mailadress = mailadress;
		this.password = password;
		this.isAdmin = isAdmin;
	}

	//class Methods
	public boolean isUsernameInList(List<User>userList) {
		for(User userInList:userList) {
			if(userInList.username.equalsIgnoreCase(this.username)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isMailAddressInList(List<User>userList) {
		for(User userInList:userList) {
			if(userInList.mailadress.equalsIgnoreCase(this.mailadress)) {
				return true;
			}
		}
		return false;
	}
	
	public List<User> addUserToList(List<User>userList){
		if(this.isUsernameInList(userList)) {
			JOptionPane.showMessageDialog(null,"User already exists!","Duplicate Warning",JOptionPane.PLAIN_MESSAGE ,new ImageIcon("./media/duplicate.png"));
		}
		else if(this.isMailAddressInList(userList)){
			JOptionPane.showMessageDialog(null,"E-Mail already exists","Duplicate Warning",JOptionPane.PLAIN_MESSAGE ,new ImageIcon("./media/email.png"));
		}
		else if(userCount >= Application.MAX_USER) {
			JOptionPane.showMessageDialog(null,"Memory limit exceeded - cannot add another user","Error Memory Limit",JOptionPane.PLAIN_MESSAGE ,new ImageIcon("./media/alert.png"));
		}else{
			userList.add(this);
			userCount++;
		}
		return userList;
	}
	
	//static Methods

	public static String userListToOutputString(List<User>userList) {
		String toReturn="";
		int linecount = 1;
		
		for(User user:userList) {
			toReturn = toReturn+linecount+". "+user.username+" (";
			if(user.isAdmin) {
				toReturn = toReturn+"Admin, ";
			}
			toReturn = toReturn+user.mailadress+")\n";
			linecount++;
		}
		return toReturn;
	}
	
}
