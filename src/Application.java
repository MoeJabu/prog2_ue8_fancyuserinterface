import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;
/* Fancy User Interface- Application
 * contains Mainclass for the Fancy User Interface
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 21.05.2021
 */
public class Application {
	
	public static final int MAX_USER=19;  
	public static List<User> userList = new ArrayList<User>();

	public static void main(String[] args) {
		/**
		 * Launch the application.
		 */
		EventQueue.invokeLater(new Runnable() {
		public void run() {
			try {
				GUI window = new GUI();
				window.frame.setVisible(true);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	});

	}

}
