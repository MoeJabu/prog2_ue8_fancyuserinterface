import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.JTextPane;


/* Fancy User Interface- GUI
 * contains GUI fuunctionality for the Fancy User Interface
 * Author: Philip Moser
 * Mail: philip.moser@edu.fh-joanneum.at
 * Last-Change: 24.05.2021
 */
public class GUI {

	JFrame frame;
	private JTextField txtUsername;
	private JTextField txtMailadress;
	private JTextField txtPassword;

	
	
	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}
	
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 858, 520);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblFancyUserManagement = new JLabel("Fancy User Management");
		lblFancyUserManagement.setFont(new Font(Font.SANS_SERIF,Font.BOLD,18));
		lblFancyUserManagement.setBounds(340, 12, 300, 44);
		frame.getContentPane().add(lblFancyUserManagement);
		
		JLabel lblListOfCurrent = new JLabel("List of current users");
		lblListOfCurrent.setBounds(68, 68, 165, 15);
		frame.getContentPane().add(lblListOfCurrent);
		
		JLabel lblUsername = new JLabel("Username:");
		lblUsername.setBounds(435, 113, 105, 15);
		frame.getContentPane().add(lblUsername);
		
		JLabel lblEmail = new JLabel("E-Mail:");
		lblEmail.setBounds(435, 140, 70, 15);
		frame.getContentPane().add(lblEmail);
		
		JLabel lblPassword = new JLabel("Password:");
		lblPassword.setBounds(435, 175, 105, 15);
		frame.getContentPane().add(lblPassword);
		
		JLabel lblAdmin = new JLabel("Admin:");
		lblAdmin.setBounds(435, 202, 70, 15);
		frame.getContentPane().add(lblAdmin);
		
		JLabel lblRegisterNewUser = new JLabel("Register new user");
		lblRegisterNewUser.setBounds(569, 68, 179, 15);
		frame.getContentPane().add(lblRegisterNewUser);
		
		
		//Image
		JLabel lblManagementimage = new JLabel(new ImageIcon("./media/management150.png"));
		lblManagementimage.setBounds(550, 300, 150, 150);
		frame.getContentPane().add(lblManagementimage);
		
		
		//Input Elements
		txtUsername = new JTextField();
		txtUsername.setText("");
		txtUsername.setBounds(540, 111, 250, 19);
		frame.getContentPane().add(txtUsername);
		txtUsername.setColumns(10);
		
		txtMailadress = new JTextField();
		txtMailadress.setText("");
		txtMailadress.setBounds(540, 141, 250, 19);
		frame.getContentPane().add(txtMailadress);
		txtMailadress.setColumns(10);
		
		txtPassword = new JPasswordField();
		txtPassword.setText("");
		txtPassword.setBounds(540, 173, 250, 19);
		frame.getContentPane().add(txtPassword);
		txtPassword.setColumns(10);
		
		JCheckBox chckbxAdmin = new JCheckBox("");
		chckbxAdmin.setBounds(536, 198, 129, 23);
		frame.getContentPane().add(chckbxAdmin);
		
		//Output Elements
		JTextPane txtpnOutput = new JTextPane();
		txtpnOutput.setText("");
		txtpnOutput.setBounds(32, 89, 316, 324);
		frame.getContentPane().add(txtpnOutput);
		txtpnOutput.setEditable(false);
		
		JProgressBar progressBar = new JProgressBar(0,Application.MAX_USER);
		progressBar.setBounds(32, 425, 267, 14);
		frame.getContentPane().add(progressBar);
		
		JLabel lblCount = new JLabel("1/"+Application.MAX_USER);
		lblCount.setBounds(317, 424, 70, 15);
		frame.getContentPane().add(lblCount);
		
		JButton btnCreateUser = new JButton("Create User");
		btnCreateUser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				User user = new User(txtUsername.getText().trim(), txtMailadress.getText().trim(), 
						txtPassword.getText().trim(),chckbxAdmin.isSelected());
				
				Application.userList = user.addUserToList(Application.userList);
				
				clearFields();
				chckbxAdmin.setSelected(false);
				
				txtpnOutput.setText(User.userListToOutputString(Application.userList));
				lblCount.setText(User.userCount+"/"+Application.MAX_USER);
				progressBar.setValue(User.userCount);		
			}
		});
		btnCreateUser.setBounds(540, 254, 165, 23);
		frame.getContentPane().add(btnCreateUser);
	}
	
	private void clearFields() {
		txtUsername.setText("");
		txtMailadress.setText("");
		txtPassword.setText("");
	}
}
